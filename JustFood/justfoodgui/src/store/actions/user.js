import * as instance from "../../axiosInstance";
import * as actionTypes from "./actionTypes";
import { history } from "../../App";

export const getCurrentUserStart = () => {
  return {
    type: actionTypes.GET_CURRENT_USER_START
  };
};
export const getCurrentUserSuccess = user => {
  return {
    type: actionTypes.GET_CURRENT_USER_SUCCESS,
    user
  };
};
export const getCurrentUserFail = error => {
  return {
    type: actionTypes.GET_CURRENT_USER_FAIL,
    error
  };
};

export const postStart = () => {
  return {
    type: actionTypes.POST_START
  };
};

export const postSuccess = () => {
  return {
    type: actionTypes.POST_SUCCESS
  };
};
export const postFail = error => {
  return {
    type: actionTypes.POST_FAIL,
    error
  };
};

export const editStart = () => {
  return {
    type: actionTypes.EDIT_START
  };
};

export const editSuccess = () => {
  return {
    type: actionTypes.EDIT_SUCCESS
  };
};
export const editFail = error => {
  return {
    type: actionTypes.EDIT_FAIL,
    error
  };
};

export const deleteStart = () => {
  return {
    type: actionTypes.DELETE_START
  };
};

export const deleteSuccess = () => {
  return {
    type: actionTypes.DELETE_SUCCESS
  };
};
export const deleteFail = error => {
  return {
    type: actionTypes.DELETE_FAIL,
    error
  };
};

export const postOrEditRecipe = (token, requestType, postObj, recipeID) => {
  if (requestType === "post") {
    return dispatch => {
      dispatch(postStart());
      console.log(postObj);

      instance.commonInstance
        .post(`/api/foodrecipe/create/`, postObj, {
          headers: {
            //   Accept: "application/json, text/plain, */*",
            Authorization: `JWT ${token}`,
            "Content-Type": "multipart/form-data"
          }
        })
        .then(res => {
          console.log(res.data);
          history.push("/");

          dispatch(postSuccess());
        })
        .catch(err => {
          dispatch(postFail(err));
        });
    };
  } else {
    return dispatch => {
      dispatch(editStart());

      instance.commonInstance
        .put(`/api/foodrecipe/${recipeID}/update/`, postObj, {
          headers: {
            Authorization: `JWT ${token}`,
            "Content-Type": "multipart/form-data"
          }
        })
        .then(res => {
          console.log(res.data);
          history.push("/");
          dispatch(editSuccess());
          //    history.push(`/recipe/${recipeID}/`);
        })
        .catch(err => {
          dispatch(editFail(err));
        });
    };
  }
};

export const deleteRecipe = (token, recipeID) => {
  return dispatch => {
    dispatch(deleteStart());

    instance.commonInstance
      .delete(`/api/foodrecipe/${recipeID}/delete/`, {
        headers: {
          Authorization: `JWT ${token}`
        }
      })
      .then(res => {
        console.log(res.data);
        history.push("/");
        dispatch(deleteSuccess());
      })
      .catch(err => {
        dispatch(deleteFail(err));
      });
  };
};
