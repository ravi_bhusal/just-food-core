import * as actionTypes from "./actionTypes";
import * as instance from "../../axiosInstance";

import * as actions from "./user";

export const authStart = () => {
  return {
    type: actionTypes.AUTH_START
  };
};

export const authSuccess = (accessToken, refreshToken) => {
  return {
    type: actionTypes.AUTH_SUCCESS,
    accessToken,
    refreshToken
  };
};

export const authFail = error => {
  return {
    type: actionTypes.AUTH_FAIL,
    error: error
  };
};

export const logout = () => {
  localStorage.removeItem("accessToken");
  localStorage.removeItem("refreshToken");

  return {
    type: actionTypes.AUTH_LOGOUT
  };
};

export const checkAuthTimeout = expirationTime => {
  return dispatch => {
    setTimeout(() => {
      dispatch(logout());
    }, expirationTime * 1000);
  };
};

export const authLogin = (username, password) => {
  return dispatch => {
    dispatch(authStart());
    dispatch(actions.getCurrentUserStart());
    instance.commonInstance
      .post(
        `/api/login/`,

        {
          username: username,
          password: password
        }
      )
      .then(res => {
        const accessToken = res.data.tokens.access;
        const refreshToken = res.data.tokens.refresh;

        const user = {
          username: res.data.username,
          id: res.data.id,
          first_name: res.data.first_name,
          last_name: res.data.last_name,
          last_login: res.data.last_login
        };

        localStorage.setItem("accessToken", accessToken);
        localStorage.setItem("refreshToken", refreshToken);

        dispatch(authSuccess(accessToken, refreshToken));
        if (user !== null || user !== undefined) {
          dispatch(actions.getCurrentUserSuccess(user));
        } else {
          dispatch(actions.getCurrentUserFail("Can't Fetch User data"));
        }
      })
      .catch(error => {
        console.log(error.request);
        dispatch(authFail(error));
      });
  };
};

export const authSignup = (username, email, password1, password2) => {
  return async dispatch => {
    dispatch(authStart());
    dispatch(actions.getCurrentUserStart());
    console.log(username, email, password1, password2);
    instance.commonInstance
      .post(
        `/api/register/`,
        {
          username: username,
          email: email,
          password1: password1,
          password2: password2
        },
        {
          headers: {
            "Content-Type": "application/json;charset=UTF-8"
          }
        }
      )
      .then(res => {
        const accessToken = res.data.tokens.access;
        const refreshToken = res.data.tokens.refresh;

        const user = {
          username: res.data.username,
          id: res.data.id,
          first_name: res.data.first_name,
          last_name: res.data.last_name,
          last_login: res.data.last_login
        };
        localStorage.setItem("accessToken", accessToken);
        localStorage.setItem("refreshToken", refreshToken);

        dispatch(authSuccess(accessToken, refreshToken));
        if (user !== null || user !== undefined) {
          dispatch(actions.getCurrentUserSuccess(user));
        } else {
          dispatch(actions.getCurrentUserFail("Can't Fetch User data"));
        }
      })
      .catch(error => {
        console.log(error.request);
        dispatch(authFail(error));
      });
  };
};
export const authCheckState = () => {
  return async dispatch => {
    const accessToken = localStorage.getItem("accessToken");
    const refreshToken = localStorage.getItem("refreshToken");

    // const accessTokenExpirationDate=
    if (accessToken === undefined || accessToken === null) {
      dispatch(authFail("token not valid"));
      dispatch(logout());
      console.log("null token");
    } else {
      const accessValidityStatus = await checkAccessTokenValidity(accessToken);

      // console.log(accessValidityStatus);
      if (accessValidityStatus !== undefined) {
        dispatch(authSuccess(accessToken, refreshToken));
        dispatch(actions.getCurrentUserStart());
        const currentuser = await getCurrentUser(accessToken);
        console.log(currentuser);
        if (currentuser !== undefined || currentuser !== null) {
          dispatch(actions.getCurrentUserSuccess(currentuser));
        } else {
          dispatch(actions.getCurrentUserFail("Couldn't fetch user"));
        }
      } else {
        console.log(accessValidityStatus);
        const newAccessToken = await getNewAccessToken(refreshToken);
        console.log(newAccessToken.data.access);
        if (
          newAccessToken.data.access !== undefined ||
          newAccessToken.data.access !== null
        ) {
          await deleteOldAccessToken();
          await storeNewAccessToken(newAccessToken.data.access);

          dispatch(authSuccess(newAccessToken.data.access, refreshToken));
          const currentuser = await getCurrentUser(newAccessToken.data.access);

          if (currentuser !== undefined || currentuser !== null) {
            dispatch(actions.getCurrentUserSuccess(currentuser));
          } else {
            dispatch(actions.getCurrentUserFail("Couldn't fetch user"));
          }
        } else {
          dispatch(authFail("token not valid"));
          dispatch(logout());
        }
      }
    }
  };
};

const checkAccessTokenValidity = accessToken => {
  const res = instance.commonInstance
    .post(
      `/api/login/verify/`,
      {
        token: accessToken
      },
      {
        headers: {
          "Content-Type": "application/json;charset=UTF-8"
        }
      }
    )
    .catch(err => {
      //    throw err;
    });
  return res;
};
const getNewAccessToken = refreshToken => {
  const res = instance.commonInstance
    .post(
      `/api/login/refresh/`,
      {
        refresh: refreshToken
      },
      {
        headers: {
          "Content-Type": "application/json"
        }
      }
    )
    .catch(error => console.log(error.request));
  return res;
};

export const getCurrentUser = async accessToken => {
  const { data } = await instance.commonInstance.get(`/api/currentuser/`, {
    headers: {
      Accept: "application/json, text/plain, */*",
      "Content-Type": "application/json;charset=utf-8",
      Authorization: `JWT ${accessToken}`
    }
  });
  return data;
};

export const deleteOldAccessToken = async () => {
  localStorage.removeItem("accessToken");
};
export const storeNewAccessToken = async accessToken => {
  localStorage.setItem("accessToken", accessToken);
};

export const storeNewRefreshToken = async refreshToken => {
  localStorage.setItem("refreshToken", refreshToken);
};
