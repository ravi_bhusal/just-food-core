import * as instance from "../../axiosInstance";
import * as actionTypes from "./actionTypes";

const getRecipeFeedStart = () => {
  return {
    type: actionTypes.GET_RECIPE_FEED_START
  };
};

const getRecipeFeedSuccess = foodrecipes => {
  return {
    type: actionTypes.GET_RECIPE_FEED_SUCCESS,
    foodrecipes
  };
};
/* const getRecipeFeedFail = error => {
  return {
    type: actionTypes.GET_RECIPE_FEED_FAIL,
    error
  };
}; */

const getRecipeFeedDetailStart = () => {
  return {
    type: actionTypes.GET_RECIPE_FEED_DETAIL_START
  };
};

const getRecipeFeedDetailSuccess = recipe => {
  return {
    type: actionTypes.GET_RECIPE_FEED_DETAIL_SUCCESS,
    recipe
  };
};
/* const getRecipeFeedDetailFail = error => {
  return {
    type: actionTypes.GET_RECIPE_FEED_DETAIL_FAIL,
    error
  };
}; */

export const getRecipeFeed = (token, searchedRecipe) => {
  return async dispatch => {
    dispatch(getRecipeFeedStart());
    // axios.defaults.headers.common = { Authorization: `JWT ${token}` };

    const recipeList = await recipeListFetch(searchedRecipe);

    const userIdList = Array.from(
      new Set(recipeList.map(({ creator }) => creator))
    );

    const userMap = await userMapFetchByIdList(userIdList);

    const recipes = recipeList.map(doc => {
      const userFetched = userMap[doc.creator] || `USER NOT FOUND`;

      return {
        ...doc,
        userFetched
      };
    });
    dispatch(getRecipeFeedSuccess(recipes));
    // return recipes;
  };
};

const recipeListFetch = async searchedRecipe => {
  try {
    const { data } = await instance.commonInstance.get(
      `http://127.0.0.1:8000/api/foodrecipe/?searchedRecipe=${searchedRecipe}`,
      {
        params: {}
      }
    );

    return data;
  } catch (error) {
    throw error;
  }
};

const userMapFetchByIdList = async idList => {
  const userList = await Promise.all(idList.map(id => userFetch({ id })));

  return userList.reduce((acc, doc) => {
    if (doc) {
      acc[doc.id] = doc;
    }

    return acc;
  }, {});
};

const userFetch = async ({ id }) => {
  try {
    const { data } = await instance.commonInstance.get(`/api/user/${id}/`);

    return data;
  } catch (error) {
    throw error;
  }
};

export const getRecipeFeedDetail = (token, recipeID) => {
  return async dispatch => {
    dispatch(getRecipeFeedDetailStart());
    //  axios.defaults.headers.common = { Authorization: `JWT ${token}` };
    const recipeDetail = await recipeFetch(recipeID);

    const userId = recipeDetail.creator;

    const userMap = await userMapFetchById(userId);

    const recipe = {
      recipeDetail,
      userMap
    };
    dispatch(getRecipeFeedDetailSuccess(recipe));
    console.log(recipe);
    //  return recipe;
  };
};

const recipeFetch = async recipeID => {
  try {
    const { data } = await instance.commonInstance.get(
      `http://127.0.0.1:8000/api/foodrecipe/` + recipeID + "/",
      {
        params: {}
      }
    );
    console.log(data);
    return data;
  } catch (error) {
    throw error;
  }
};

const userDetailFetch = async ({ id }) => {
  try {
    const { data } = await instance.commonInstance.get(`/api/user/${id}/`);

    return data;
  } catch (error) {
    throw error;
  }
};

const userMapFetchById = async id => {
  const user = await userDetailFetch({ id });
  console.log(user);
  return user;
};
