import * as actionTypes from "../actions/actionTypes";
import { updateObject } from "../utility";

const initialState = {
  foodrecipes: [],
  userFeedError: null,
  loading: false,

  creatorId: null,
  creatorName: null,
  recipeName: null,
  recipeDescription: null,
  recipeImage: null
};

const getRecipeFeedStart = (state, action) => {
  return updateObject(state, {
    userFeedError: null,
    loading: true
  });
};

const getRecipeFeedSuccess = (state, action) => {
  return updateObject(state, {
    foodrecipes: action.foodrecipes,
    userFeedError: null,
    loading: false
  });
};
const getRecipeFeedFail = (state, action) => {
  return updateObject(state, {
    userFeedError: action.error,
    loading: false
  });
};

const getRecipeFeedDetailStart = (state, action) => {
  return updateObject(state, {
    userFeedError: null,
    loading: true
  });
};

const getRecipeFeedDetailSuccess = (state, action) => {
  return updateObject(state, {
    creatorId: action.recipe.userMap.id,
    creatorName: action.recipe.userMap.username,
    recipeName: action.recipe.recipeDetail.name,
    recipeDescription: action.recipe.recipeDetail.description,
    recipeImage: action.recipe.recipeDetail.image,

    userFeedError: null,
    loading: false
  });
};
const getRecipeFeedDetailFail = (state, action) => {
  return updateObject(state, {
    userFeedError: action.error,
    loading: false
  });
};

const reducer = (state = initialState, action) => {
  switch (action.type) {
    case actionTypes.GET_RECIPE_FEED_START:
      return getRecipeFeedStart(state, action);
    case actionTypes.GET_RECIPE_FEED_SUCCESS:
      return getRecipeFeedSuccess(state, action);
    case actionTypes.GET_RECIPE_FEED_FAIL:
      return getRecipeFeedFail(state, action);

    case actionTypes.GET_RECIPE_FEED_DETAIL_START:
      return getRecipeFeedDetailStart(state, action);
    case actionTypes.GET_RECIPE_FEED_DETAIL_SUCCESS:
      return getRecipeFeedDetailSuccess(state, action);
    case actionTypes.GET_RECIPE_FEED_DETAIL_FAIL:
      return getRecipeFeedDetailFail(state, action);

    default:
      return state;
  }
};

export default reducer;
