import * as actionTypes from "../actions/actionTypes";
import { updateObject } from "../utility";

const initialState = {
  userError: null,
  loading: false,
  username: null,
  userId: null,
  first_name: null,
  last_name: null,
  last_login: null
};

const getCurrentUserStart = (state, action) => {
  return updateObject(state, {
    userError: null,
    loading: true
  });
};

const getCurrentUserSuccess = (state, action) => {
  return updateObject(state, {
    username: action.user.username,
    userId: action.user.id,
    first_name: action.user.first_name,
    last_name: action.user.last_name,
    last_login: action.user.last_login,
    userError: null,
    loading: true
  });
};

const getCurrentUserFail = (state, action) => {
  return updateObject(state, {
    userError: null,
    loading: true
  });
};

const postStart = (state, action) => {
  return updateObject(state, {
    userError: null,
    loading: true
  });
};

const postSuccess = (state, action) => {
  return updateObject(state, {
    userError: null,
    loading: false
  });
};

const postFail = (state, action) => {
  return updateObject(state, {
    userError: action.error,
    loading: false
  });
};

const editStart = (state, action) => {
  return updateObject(state, {
    userError: null,
    loading: true
  });
};

const editSuccess = (state, action) => {
  return updateObject(state, {
    userError: null,
    loading: false
  });
};

const editFail = (state, action) => {
  return updateObject(state, {
    userError: action.error,
    loading: false
  });
};

const deleteStart = (state, action) => {
  return updateObject(state, {
    userError: null,
    loading: true
  });
};

const deleteSuccess = (state, action) => {
  return updateObject(state, {
    userError: null,
    loading: false
  });
};

const deleteFail = (state, action) => {
  return updateObject(state, {
    userError: action.error,
    loading: false
  });
};

const reducer = (state = initialState, action) => {
  switch (action.type) {
    case actionTypes.GET_CURRENT_USER_START:
      return getCurrentUserStart(state, action);
    case actionTypes.GET_CURRENT_USER_SUCCESS:
      return getCurrentUserSuccess(state, action);
    case actionTypes.GET_CURRENT_USER_FAIL:
      return getCurrentUserFail(state, action);

    case actionTypes.POST_START:
      return postStart(state, action);
    case actionTypes.POST_SUCCESS:
      return postSuccess(state, action);
    case actionTypes.POST_FAIL:
      return postFail(state, action);

    case actionTypes.EDIT_START:
      return editStart(state, action);
    case actionTypes.EDIT_SUCCESS:
      return editSuccess(state, action);
    case actionTypes.EDIT_FAIL:
      return editFail(state, action);

    case actionTypes.DELETE_START:
      return deleteStart(state, action);
    case actionTypes.DELETE_SUCCESS:
      return deleteSuccess(state, action);
    case actionTypes.DELETE_FAIL:
      return deleteFail(state, action);

    default:
      return state;
  }
};

export default reducer;
