import React from "react";
import { Layout, Menu } from "antd";

import { Link } from "react-router-dom";

import { connect } from "react-redux";
import * as actions from "../store/actions/auth";

const { Header, Content, Footer } = Layout;

class CustomLayout extends React.Component {
  render() {
    return (
      <Layout className="layout">
        <Header>
          <div className="logo" />
          <Menu
            theme="dark"
            mode="horizontal"
            defaultSelectedKeys={["2"]}
            style={{ lineHeight: "64px", padding: "0 200px" }}
          >
            {this.props.isAuthenticated ? (
              <Menu.Item key="3" onClick={this.props.logout}>
                Logout
              </Menu.Item>
            ) : (
              <Menu.Item key="2">
                <Link to="/login">Login</Link>
              </Menu.Item>
            )}
            <Menu.Item key="1">
              <Link to="/">Posts</Link>
            </Menu.Item>
          </Menu>
        </Header>
        <Content style={{ padding: "0 250px" }}>
          <div style={{ background: "#fff", padding: 24, minHeight: 850 }}>
            {this.props.children}
          </div>
        </Content>
        <Footer
          style={{
            textAlign: "center",
            marginBottom: 10
          }}
        >
          Practice Project by Ravi Bhusal
        </Footer>
      </Layout>
    );
  }
}

const mapDispatchToProps = dispatch => {
  return {
    logout: () => dispatch(actions.logout())
  };
};

export default connect(
  null,
  mapDispatchToProps
)(CustomLayout);
