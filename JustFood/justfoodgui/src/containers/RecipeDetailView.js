import React from "react";

import CustomForm from "../components/Form";

import { Card, Button, Form } from "antd";

import * as actions from "../store/actions/userFeed";

import * as userActions from "../store/actions/user";

import { connect } from "react-redux";

const { Meta } = Card;
class RecipeDetail extends React.Component {
  state = {
    recipe: {}
  };

  componentDidMount() {
    const recipeID = this.props.match.params.recipeID;
    this.props.getRecipeFeedDetail(this.props.token, recipeID);
    console.log("here", recipeID);
    console.log(recipeID);
  }

  componentWillReceiveProps(nextProps) {
    if (nextProps.token !== this.props.token) {
      if (nextProps.token !== undefined && nextProps.token !== null) {
        const recipeID = this.props.match.params.recipeID;
        this.props.getRecipeFeedDetail(nextProps.token, recipeID);
      }
    }
    console.log(this.props.postCreatorId);
    console.log(this.props.userId);
    //this.setState(nextProps);
  }

  handleDelete = (event, recipeID) => {
    event.preventDefault();
    this.props.deletePost(this.props.token, recipeID);
  };

  handleCheck = () => {
    console.log(this.props.image);
    console.log(this.props.name);
    console.log(this.props.postCreatorId);
    console.log(this.props.userId);
  };
  render() {
    return (
      <div>
        <Card
          style={{ width: 500 }}
          cover={<img alt="example" src={this.props.image} />}
        >
          <Meta title={this.props.name} description={this.props.description} />
        </Card>

        <br />
        {this.props.postCreatorId === this.props.userId ? (
          <div>
            <h2>Update</h2>
            <CustomForm
              requestType="put"
              recipeID={this.props.match.params.recipeID}
              btnText="Update"
            />
            <Form
              onSubmit={event =>
                this.handleDelete(event, this.props.match.params.recipeID)
              }
            >
              <Button type="danger" htmlType="submit">
                Delete
              </Button>
            </Form>
          </div>
        ) : (
          <div />
        )}
      </div>
    );
  }
}

const mapStateToProps = state => {
  return {
    token: state.auth.accessToken,
    postCreatorId: state.userFeed.creatorId,
    userId: state.user.userId,
    creatorName: state.userFeed.creatorName,
    name: state.userFeed.recipeName,
    description: state.userFeed.recipeDescription,
    image: state.userFeed.recipeImage,
    loading: state.userFeed.loading
  };
};

const mapDispatchToProps = dispatch => {
  return {
    getRecipeFeedDetail: (token, recipeID) =>
      dispatch(actions.getRecipeFeedDetail(token, recipeID)),
    deletePost: (token, recipeID) =>
      dispatch(userActions.deleteRecipe(token, recipeID))
  };
};

export default connect(
  mapStateToProps,
  mapDispatchToProps
)(RecipeDetail);
