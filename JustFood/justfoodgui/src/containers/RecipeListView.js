import React from "react";

import FoodRecipe from "../components/FoodRecipe";
import CustomForm from "../components/Form";

import * as actions from "../store/actions/userFeed";

import { connect } from "react-redux";

import { Input } from "antd";

const { Search } = Input;

class FoodRecipeList extends React.Component {
  state = {
    foodrecipes: []
  };

  componentDidMount() {
    var searchedRecipe = "";
    this.props.getRecipeFeed(this.props.token, searchedRecipe);
  }

  componentWillReceiveProps(nextProps) {
    if (nextProps.token !== this.props.token) {
      if (nextProps.token !== undefined && nextProps.token !== null) {
        var searchedRecipe = "";
        this.props.getRecipeFeed(nextProps.token, searchedRecipe);
      }
    }
    //this.setState(nextProps);
  }

  handleSearch = value => {
    this.props.getRecipeFeed(this.props.token, value);
  };

  render() {
    return (
      <div>
        <Search
          placeholder="Search for recipes..."
          onSearch={value => this.handleSearch(value)}
          style={{ width: 200 }}
        />
        <br />
        <FoodRecipe data={this.props.foodrecipes} />
        <br />
        {this.props.isAuthenticated ? (
          <div>
            <h2>Share a recipe.</h2>
            <CustomForm requestType="post" recipeID={null} btnText="Create" />
          </div>
        ) : (
          <div />
        )}
      </div>
    );
  }
}

const mapStateToProps = state => {
  return {
    foodrecipes: state.userFeed.foodrecipes,
    loading: state.userFeed.loading,
    token: state.auth.accessToken,
    isAuthenticated: state.auth.isAuthenticated
  };
};

const mapDispatchToProps = dispatch => {
  return {
    getRecipeFeed: (token, searchedRecipe) =>
      dispatch(actions.getRecipeFeed(token, searchedRecipe))
  };
};

export default connect(
  mapStateToProps,
  mapDispatchToProps
)(FoodRecipeList);
