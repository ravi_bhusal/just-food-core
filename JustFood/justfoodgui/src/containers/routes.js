import React from "react";
import { Route } from "react-router-dom";

import RecipeList from "./RecipeListView";
import RecipeDetail from "./RecipeDetailView";
import Login from "./Login";
import Signup from "./Signup";

import PublicRoute from "./PublicRoute";

class BaseRouter extends React.Component {
  render() {
    return (
      <div>
        <Route exact path="/" component={RecipeList} />
        <Route exact path="/recipe/:recipeID/" component={RecipeDetail} />
        <PublicRoute
          exact
          path="/login/"
          component={Login}
          isAuthenticated={this.props.isAuthenticated}
        />
        <PublicRoute
          exact
          path="/signup/"
          component={Signup}
          isAuthenticated={this.props.isAuthenticated}
        />
      </div>
    );
  }
}

export default BaseRouter;
