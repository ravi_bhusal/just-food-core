import React, { Component } from "react";

//import { BrowserRouter as Router } from "react-router-dom";

import { Router } from "react-router";

import { connect } from "react-redux";
import BaseRouter from "./containers/routes";
import "antd/dist/antd.css";
import * as actions from "./store/actions/auth";

import CustomLayout from "./containers/Layout";
import createBrowserHistory from "history/createBrowserHistory";

export const history = createBrowserHistory();

class App extends Component {
  componentDidMount() {
    this.props.onTryAutoSignUp();
  }
  render() {
    return (
      <div>
        <Router history={history}>
          <CustomLayout {...this.props}>
            <BaseRouter {...this.props} />
          </CustomLayout>
        </Router>
      </div>
    );
  }
}

const mapStateToProps = state => {
  return {
    isAuthenticated: state.auth.isAuthenticated
  };
};

const mapDispatchToProps = dispatch => {
  return {
    onTryAutoSignUp: () => dispatch(actions.authCheckState())
  };
};

export default connect(
  mapStateToProps,
  mapDispatchToProps
)(App);
