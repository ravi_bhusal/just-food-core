import React from "react";
import { Form, Input, Button } from "antd";

import * as actions from "../store/actions/user";

import { connect } from "react-redux";
const FormItem = Form.Item;

class CustomForm extends React.Component {
  state = {
    name: "",
    description: "",
    image: null
  };

  componentDidMount() {
    console.log("form", this.props.recipeID);
  }

  handleImageChange = e => {
    this.setState({
      image: e.target.files[0]
    });
  };

  handleFormSubmit = (event, requestType, recipeID) => {
    event.preventDefault();
    const postObj = new FormData();

    postObj.append("creator", this.props.userId);
    postObj.append("image", this.state.image);
    postObj.append("name", event.target.elements.name.value);
    postObj.append("description", event.target.elements.description.value);

    console.log(postObj);
    this.props.postOrEditRecipe(
      this.props.token,
      requestType,
      postObj,
      recipeID
    );
  };

  render() {
    return (
      <div>
        <Form
          onSubmit={event =>
            this.handleFormSubmit(
              event,
              this.props.requestType,
              this.props.recipeID
            )
          }
        >
          <FormItem label="Name">
            <Input name="name" placeholder="Name..." />
          </FormItem>

          <FormItem label="description">
            <Input
              name="description"
              placeholder="Write some content here..."
            />
          </FormItem>

          <input
            type="file"
            id="image"
            accept="image/png, image/jpeg"
            onChange={this.handleImageChange}
            required
          />
          <FormItem>
            <Button type="primary" htmlType="submit">
              {this.props.btnText}
            </Button>
          </FormItem>
        </Form>
      </div>
    );
  }
}

const mapStateToProps = state => {
  return {
    token: state.auth.accessToken,
    userId: state.user.userId
  };
};

const mapDispatchToProps = dispatch => {
  return {
    postOrEditRecipe: (token, requestType, postObj, recipeID) =>
      dispatch(actions.postOrEditRecipe(token, requestType, postObj, recipeID))
  };
};

export default connect(
  mapStateToProps,
  mapDispatchToProps
)(CustomForm);
