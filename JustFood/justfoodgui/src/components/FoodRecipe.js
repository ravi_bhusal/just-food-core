import React from "react";
import { List, Avatar } from "antd";

const FoodRecipe = props => {
  return (
    <List
      itemLayout="vertical"
      size="large"
      pagination={{
        onChange: page => {
          console.log(page);
        },
        pageSize: 10
      }}
      dataSource={props.data}
      renderItem={item => (
        <List.Item
          key={item.name}
          // actions={[<IconText type="star-o" text="156" />]}
          extra={<img width={272} alt="logo" src={item.image} />}
        >
          <List.Item.Meta
            avatar={<Avatar src={item.image} />}
            title={<a href={`/recipe/${item.id}/`}>{item.name}</a>}
          />
          <p style={{ fontStyle: "italics" }}>
            shared by {item.userFetched.username}
          </p>
          {item.description}
        </List.Item>
      )}
    />
  );
};

export default FoodRecipe;
