
from rest_framework import viewsets
from rest_framework.generics import ListCreateAPIView,ListAPIView,CreateAPIView,RetrieveUpdateAPIView,RetrieveAPIView,DestroyAPIView


from core.models import FoodRecipe
from .serializers import FoodRecipeSerializer,RegisterUserSerializer,LoginUserSerializer,UserSerializer


from rest_framework.parsers import MultiPartParser, FormParser, FileUploadParser
from django.contrib.auth.models import User

from rest_framework import permissions
from rest_framework.decorators import api_view, list_route
from rest_framework.response import Response
from rest_framework.authentication import SessionAuthentication, BasicAuthentication
from rest_framework_simplejwt.authentication import JWTAuthentication
from django.http import JsonResponse, HttpResponse
from rest_framework_simplejwt.views import TokenObtainPairView
from django.contrib.auth.models import update_last_login

from django.contrib.auth import authenticate,login


class IsOwnerOrReadOnly(permissions.BasePermission):
    """
    Object-level permission to only allow owners of an object to edit it.
    Assumes the model instance has an `owner` attribute.
    """

    def has_object_permission(self, request, view, obj):
        # Read permissions are allowed to any request,
        # so we'll always allow GET, HEAD or OPTIONS requests.
        if request.method in permissions.SAFE_METHODS:
            return True

        # Instance must have an attribute named `owner`.
        return obj.creator == request.user


class FoodRecipeListView(ListAPIView):
    queryset = FoodRecipe.objects.all()
    serializer_class = FoodRecipeSerializer
    authentication_classes=(JWTAuthentication,)
    permission_classes=(permissions.AllowAny,)

    def get_queryset(self):
        
        queryset =  FoodRecipe.objects.all().order_by('-shared_date')
        searched_recipe = self.request.query_params.get('searchedRecipe', None)

        


        if searched_recipe is not "":
            queryset = FoodRecipe.objects.all().filter(name__icontains=searched_recipe).order_by('-shared_date')
           
        return queryset

class FoodRecipeCreateView(CreateAPIView):
    parser_classes = (MultiPartParser, FormParser,FileUploadParser)
    queryset = FoodRecipe.objects.all()
    serializer_class = FoodRecipeSerializer
    authentication_classes=(JWTAuthentication,SessionAuthentication,BasicAuthentication)
    permission_classes=(permissions.IsAuthenticated,)

class FoodRecipeDetailView(RetrieveAPIView):
    queryset = FoodRecipe.objects.all()
    serializer_class = FoodRecipeSerializer
    authentication_classes=(JWTAuthentication,)
    permission_classes=(permissions.AllowAny,)

class FoodRecipeUpdateView(RetrieveUpdateAPIView):
    parser_classes = (MultiPartParser, FormParser,FileUploadParser)
    queryset = FoodRecipe.objects.all()
    serializer_class = FoodRecipeSerializer
    authentication_classes=(JWTAuthentication,)
    permission_classes=(IsOwnerOrReadOnly,)

class FoodRecipeDeleteView(DestroyAPIView):
    queryset=FoodRecipe.objects.all()
    serializer_class = FoodRecipeSerializer
    authentication_classes=(JWTAuthentication,)
    permission_classes=(IsOwnerOrReadOnly,)



class UserDetailView(RetrieveAPIView):
    serializer_class = UserSerializer
    queryset = User.objects.all()
    permission_classes = (permissions.AllowAny,)


class CurrentUser(RetrieveUpdateAPIView):
    authentication_classes=(JWTAuthentication,)
    permission_classes = (permissions.IsAuthenticated,)
    serializer_class=UserSerializer
    queryset = ''
    
   
    def get(self,request,format=None):
        serializer=UserSerializer(request.user)
        content=serializer.data     
        return Response(content)
    
    def put(self,request,format=None,pk=None):
        
        serializer=UserSerializer(request.user,data=request.data,partial=True)
        if serializer.is_valid():
            serializer.save()
            return JsonResponse(status=201, data=serializer.data)
        return JsonResponse(status=400, data=serializer.errors)
    
    def patch(self,request,format=None,pk=None):
        serializer=UserSerializer(request.user,data=request.data,partial=True)
        if serializer.is_valid(raise_exception=True):
            serializer.save()
            return JsonResponse(status=201, data=serializer.data)
        return JsonResponse(status=400, data=serializer.errors)

class RegisterUserView(CreateAPIView):
    authentication_classes = []
    queryset = User.objects.all()
    serializer_class=RegisterUserSerializer
    permission_classes = (permissions.AllowAny,)



    def create(self, request, *args, **kwargs):
        serializer = RegisterUserSerializer(data=request.data)
        if serializer.is_valid():
            self.perform_create(serializer)
            username = serializer.data.get('username', None)
            password = serializer.data.get('password', None)

            user = authenticate(username=username, password=password)
            
          #  login(request, user,backend='django.contrib.auth.backends.ModelBackend')
           # update_last_login(None, user)
                   
                    
            
            return Response(serializer.data, status=201)
        return Response(serializer.errors, status=400)

class LoginUserView(CreateAPIView):
    authentication_classes = []
    queryset=User.objects.all()
    serializer_class=LoginUserSerializer
    permission_classes=(permissions.AllowAny,)

   

    def post(self, request, format=None):
        
        data = request.data
        username = data.get('username', None)
        password = data.get('password', None)

        user = authenticate(username=username, password=password)

        if user is not None:
            if user.is_active:
                login(request, user)
                update_last_login(None, user)
                serializer=LoginUserSerializer(request.user)
                content=serializer.data     
                return Response(content,status=200)
            else:
                return Response(status=404)
        else:
            return Response(status=404)
    
    #def get(self,request,format=None):
        