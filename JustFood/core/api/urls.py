from django.urls import path, include

from core.api.views import FoodRecipeListView,FoodRecipeCreateView,FoodRecipeDetailView,FoodRecipeUpdateView,FoodRecipeDeleteView,RegisterUserView,LoginUserView,CurrentUser, UserDetailView 


from rest_framework_simplejwt.views import (
    TokenObtainPairView,
    TokenRefreshView,
    TokenVerifyView
)

urlpatterns = [

    
    path('foodrecipe/', FoodRecipeListView.as_view()),
    path('foodrecipe/create/',FoodRecipeCreateView.as_view()),
    path('foodrecipe/<pk>/',FoodRecipeDetailView.as_view()),
    path('foodrecipe/<pk>/update/',FoodRecipeUpdateView.as_view()),
    path('foodrecipe/<pk>/delete/',FoodRecipeDeleteView.as_view()),

    path('user/<pk>/', UserDetailView.as_view()),

    path('currentuser/',CurrentUser.as_view()),

    path('login/', LoginUserView.as_view(), name='token_obtain_pair'),
    path('login/refresh/', TokenRefreshView.as_view(), name='token_refresh'),

    path('register/',RegisterUserView.as_view(), name='register_user_token' ),
    path('login/verify/', TokenVerifyView.as_view(), name='token_verify'),
   
]
