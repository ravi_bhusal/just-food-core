from rest_framework import serializers

from core.models import FoodRecipe

from django.contrib.auth.models import User

from rest_framework_simplejwt.serializers import TokenObtainPairSerializer


from rest_framework_simplejwt.tokens import AccessToken, RefreshToken



class FoodRecipeSerializer(serializers.ModelSerializer):
    class Meta:
        model=FoodRecipe
        fields=('id','creator','name','description','image')


class UserSerializer(serializers.ModelSerializer):
    class Meta:
        model = User
        fields = ('id', 'username','first_name','last_name', 'last_login')


class LoginUserSerializer(serializers.ModelSerializer):
    '''Serializer for fetching token'''
    tokens = serializers.SerializerMethodField()
    password = serializers.CharField(write_only=True)
    class Meta:
        model = User
        fields = ('tokens','id','username','first_name','last_name','last_login', 'password', 'email')
        read_only_fields = ( 'first_name','last_name','last_login')
    
    def get_tokens(self, user):
        tokens = RefreshToken.for_user(user)
        refresh = str(tokens)
        access = str(tokens.access_token)
        data = {
        "refresh": refresh,
        "access": access,
        }
        return data

   
    
    
class RegisterUserSerializer(serializers.ModelSerializer):
    """Serializer for creating user objects."""

    tokens = serializers.SerializerMethodField()
    password1 = serializers.CharField(write_only=True)
    password2 = serializers.CharField(write_only=True)
    


    class Meta:
        model = User
        fields = ('tokens','id','username','first_name','last_name','last_login', 'password1','password2', 'email')
        read_only_fields = ( 'first_name','last_name','last_login')

    def validate(self, data):
        if not data.get('password1') or not data.get('password2'):
            raise serializers.ValidationError("Please enter a password and "
                "confirm it.")

        if data.get('password1') != data.get('password2'):
            raise serializers.ValidationError("Those passwords don't match.")

        return data

    def get_tokens(self, user):
        tokens = RefreshToken.for_user(user)
        refresh = str(tokens)
        access = str(tokens.access_token)
        data = {
            "refresh": refresh,
            "access": access
        }
        return data

    def create(self, validated_data):
        user = User.objects.create_user(
            username=validated_data['username']
        )
        user.set_password(validated_data['password1'])
        user.save()    
        return user