from django.db import models
from django.db.models import Manager
from django.db.models.query import QuerySet
from django.contrib.auth.models import User
# Create your models here.

from datetime import datetime







class FoodRecipe(models.Model):
    creator=models.ForeignKey(User,on_delete=models.CASCADE)
    name=models.CharField(max_length=40,help_text="Name of your Food")
    description=models.CharField(max_length=2000,help_text="Ingredients and description")
    image=models.ImageField(upload_to="foodimages",null=True,blank=True)
    shared_date=models.DateTimeField(default=datetime.now, blank=True)


    def __str__(self):
        return self.name

