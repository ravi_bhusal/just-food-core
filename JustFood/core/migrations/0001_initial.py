# Generated by Django 2.2.2 on 2019-06-17 06:26

from django.conf import settings
from django.db import migrations, models
import django.db.models.deletion


class Migration(migrations.Migration):

    initial = True

    dependencies = [
        migrations.swappable_dependency(settings.AUTH_USER_MODEL),
    ]

    operations = [
        migrations.CreateModel(
            name='FoodRecipe',
            fields=[
                ('id', models.AutoField(auto_created=True, primary_key=True, serialize=False, verbose_name='ID')),
                ('name', models.CharField(help_text='Name of your Food', max_length=40)),
                ('tags', models.CharField(blank='True', help_text='Continental, Italian, Indian, etc.', max_length=100, null='True')),
                ('ingredients', models.CharField(help_text='list of food ingredients', max_length=400)),
                ('description', models.CharField(help_text='Recipe detailed description', max_length=2000)),
                ('image', models.ImageField(blank=True, null=True, upload_to='foodimages')),
                ('creator', models.ForeignKey(on_delete=django.db.models.deletion.CASCADE, to=settings.AUTH_USER_MODEL)),
            ],
        ),
    ]
